'use strict';

let fs = require('fs');
let path = require('path');
let {createInterface} = require('readline');
const rl = createInterface(process.stdin, process.stdout);

// folder with all pages
const PAGES_DIR = path.join(__dirname, 'src/pages');

// default content for files in new page
const fileSources = {
	ejs: `<% locals.setLayout('main.ejs') -%>
<% locals.block('title', '{pageName}') -%>
<% locals.block('page', '{pageName}') -%>\n\n`,
};

function validatePageName(pageName) {
	return new Promise((resolve, reject) => {
		const isValid = /^(\d|\w|-)+$/.test(pageName);

		if (isValid) {
			resolve(isValid);
		} else {
			const errMsg = (
				`ERR>>> An incorrect page name '${pageName}'\n` +
				`ERR>>> A page name must include letters, numbers & the minus symbol.`
			);
			reject(errMsg);
		}
	});
}

// function directoryExist(pagePath, pageName) {
// 	return new Promise((resolve, reject) => {
// 		fs.stat(pagePath, notExist => {
// 			if (notExist) {
// 				resolve();
// 			} else {
// 				reject(`ERR>>> The page '${pageName}' already exists.`);
// 			}
// 		});
// 	});
// }

function createDir(dirPath) {
	return new Promise((resolve, reject) => {
		fs.mkdir(dirPath, err => {
			if (err) {
				reject(`ERR>>> Failed to create a folder '${dirPath}'`);
			} else {
				resolve();
			}
		});
	});
}

function createFiles(pagesPath, pageName) {
	const promises = [];
	Object.keys(fileSources).forEach(ext => {
		const fileSource = fileSources[ext].replace(/\{pageName}/g, pageName);
		const filename = `${pageName}.${ext}`;
		const filePath = path.join(pagesPath, filename);

		promises.push(
				new Promise((resolve, reject) => {
					fs.writeFile(filePath, fileSource, 'utf8', err => {
						if (err) {
							reject(`ERR>>> Failed to create a file '${filePath}'`);
						} else {
							resolve();
						}
					});
				})
		);
	});

	return Promise.all(promises);
}

function getFiles(pagePath) {
	return new Promise((resolve, reject) => {
		fs.readdir(pagePath, (err, files) => {
			if (err) {
				reject(`ERR>>> Failed to get a file list from a folder '${pagePath}'`);
			} else {
				resolve(files);
			}
		});
	});
}

function printErrorMessage(errText) {
	console.log(errText);
	rl.close();
}

function initMakePage(candidatePageName) {
	const pageNames = candidatePageName.trim().split(/\s+/);

	const makePage = pageName => {
		const pagePath = path.join(PAGES_DIR);

		return validatePageName(pageName)
			// .then(() => directoryExist(pagePath, pageName))
			// .then(() => createDir(pagePath))
			.then(() => createFiles(pagePath, pageName))
			.then(() => getFiles(pagePath))
			.then(files => {
				const line = '-'.repeat(48 + pageName.length);
				console.log(line);
				console.log(`The page has just been created in 'app/pages/${pageName}'`);
				console.log(line);

				// Displays a list of files created
				files.forEach(file => console.log(file));

				rl.close();
			});
	};

	if (pageNames.length === 1) {
		return makePage(pageNames[0]);
	}

	const promises = pageNames.map(name => makePage(name));
	return Promise.all(promises);
}

//
// Start here
//

// Command line arguments
const pageNameFromCli = process.argv
		.slice(2)
		// join all arguments to one string (to simplify the capture user input errors)
		.join(' ');

// If the user pass the name of the page in the command-line options
// that create a page. Otherwise - activates interactive mode
if (pageNameFromCli !== '') {
	initMakePage(pageNameFromCli).catch(printErrorMessage);
} else {
	rl.setPrompt('Page(s) name: ');
	rl.prompt();
	rl.on('line', (line) => {
		initMakePage(line).catch(printErrorMessage);
	});
}
