const gulp = require('gulp')
const watch = require('gulp-watch')
const combiner = require('stream-combiner2').obj
const ejs = require('gulp-ejs-monster')
const sass = require('gulp-sass')
const sassGlob = require('gulp-sass-glob')
const cleanCSS = require('gulp-clean-css')
const notify = require('gulp-notify')
const sourcemaps = require('gulp-sourcemaps')
const postcss = require('gulp-postcss')
const autoPrefixer = require('autoprefixer')
const flexBugsFixes = require('postcss-flexbugs-fixes')
const gcmq = require('gulp-group-css-media-queries')
const gulpIf = require('gulp-if')
const del = require('del')
const browserSync = require('browser-sync')
const rename = require('gulp-rename')

sass.compiler = require('node-sass');



let flags = {
	production: false
}

const PATHS = {
	src: 'src',
	dest: 'dist',
	libs: 'src/libs',
	srcAssets: 'src/assets',
	destAssets: 'dist/assets'
}



/* Views
****************************************************/
gulp.task('views', function (done) {

	// index
	combiner(
		gulp.src(`${PATHS.src}/index.ejs`),
		ejs({
			layouts: 'pages/layouts/',
		}).on('error', ejs.preventCrash),
		gulp.dest(PATHS.dest)
	).on('error', notify.onError())

	// pages
	combiner(
		gulp.src(`${PATHS.src}/pages/*.ejs`),
		ejs({
			layouts: `${PATHS.src}/pages/layouts`,
			widgets: `${PATHS.src}/modules`,
			compileDebug: true
		}).on('error', ejs.preventCrash),
		gulp.dest(`${PATHS.dest}/pages`)
	).on('error', notify.onError())

	done()
})



/* Styles
****************************************************/
gulp.task('styles', function (done) {
	let processors = [
		flexBugsFixes()
	]

	if ( flags.production ) {
		processors.push(
			autoPrefixer('> 1%, Last 2 versions, iOS 8')
		)
	}

	combiner(
		gulp.src(`${PATHS.src}/app.scss`),
		gulpIf(!flags.production, sourcemaps.init()),
		sassGlob(),
		sass({
			includePaths: [PATHS.src]
		}),
		postcss(processors),
		gcmq(),
		gulpIf(flags.production, cleanCSS()),
		gulpIf(!flags.production, sourcemaps.write()),
		gulp.dest(`${PATHS.destAssets}/css`)
	).on('error', notify.onError())

	done()
})



/* Images of modules
****************************************************/
gulp.task('imagesOfModules', function () {
	return gulp.src(`${PATHS.src}/modules/*/images/**/*.*`)
		.pipe(rename(function (path) {
			let separator = '/';

			if ( !~path.dirname.indexOf('/') ) {
				separator = '\\';
			}
			
			path.dirname = path.dirname.split(separator).filter(function (value, index) {
				if ( value && value != 'images' ) return value
			}).join('/')
		}))
		.pipe(gulp.dest(`${PATHS.destAssets}/images/modules`))
})



/* Assets
****************************************************/
gulp.task('assets', function () {
	return gulp.src(`${PATHS.srcAssets}/**/*.*`)
		.pipe(gulp.dest(PATHS.destAssets))
})



/* Server
****************************************************/
gulp.task('server', function (done) {
	browserSync.init({ server: PATHS.dest })
	browserSync.watch(PATHS.dest + '/**/*.*').on('change', browserSync.reload)

	done()
})



/* Watch
****************************************************/
gulp.task('watch', function (done) {
	watch(`${PATHS.src}/**/*.ejs`, gulp.series('views'))
	watch(`${PATHS.src}/**/*.scss`, gulp.series('styles'))
	watch(`${PATHS.src}/modules/*/images/**/*.*`, gulp.series('imagesOfModules'))
	watch(`${PATHS.srcAssets}/**/*.*`, gulp.series('assets'))

	done()
})



/* Build
****************************************************/
gulp.task('build', gulp.parallel('views', 'styles', 'imagesOfModules', 'assets'))



/* Clean
****************************************************/
gulp.task('clean', function () {
	return del('./dist')
})



/* Prod
****************************************************/
gulp.task('prod', function (done) {
	flags.production = true
	gulp.series('clean', 'build')

	done()
})



/* Default
****************************************************/
gulp.task('default', gulp.series('build', 'server', 'watch'))
