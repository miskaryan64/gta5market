'use strict';

let fs = require('fs');
let path = require('path');
let {createInterface} = require('readline');
const rl = createInterface(process.stdin, process.stdout);

// folder with all modules
const BLOCKS_DIR = path.join(__dirname, 'src/modules');
// const MODULES_JS_PATH = path.join(__dirname, 'src/modules.js');
// const MODULES_STYL_PATH = path.join(__dirname, 'src/modules.styl');

// default content for files in new module
const fileSources = {
	ejs: `.{moduleName}`,
	scss: `.{moduleName} {}`
};

function validateBlockName(moduleName) {
	return new Promise((resolve, reject) => {
		const isValid = /^(\d|\w|-)+$/.test(moduleName);

		if (isValid) {
			resolve(isValid);
		} else {
			const errMsg = (
				`ERR>>> An incorrect module name '${moduleName}'\n` +
				`ERR>>> A module name must include letters, numbers & the minus symbol.`
			);
			reject(errMsg);
		}
	});
}

function directoryExist(modulePath, moduleName) {
	return new Promise((resolve, reject) => {
		fs.stat(modulePath, notExist => {
			if (notExist) {
				resolve();
			} else {
				reject(`ERR>>> The module '${moduleName}' already exists.`);
			}
		});
	});
}

function createDir(dirPath) {
	return new Promise((resolve, reject) => {
		fs.mkdir(dirPath, err => {
			if (err) {
				reject(`ERR>>> Failed to create a folder '${dirPath}'`);
			} else {
				resolve();
			}
		});
	});
}

function createFiles(modulesPath, moduleName) {
	const promises = [];
	Object.keys(fileSources).forEach(ext => {
		const fileSource = fileSources[ext].replace(/\{moduleName}/g, moduleName);
		const filename = `${moduleName}.${ext}`;
		const filePath = path.join(modulesPath, filename);

		promises.push(
				new Promise((resolve, reject) => {
					fs.writeFile(filePath, fileSource, 'utf8', err => {
						if (err) {
							reject(`ERR>>> Failed to create a file '${filePath}'`);
						} else {
							resolve();
						}
					});
				})
		);
	});

	return Promise.all(promises);
}

function getFiles(modulePath) {
	return new Promise((resolve, reject) => {
		fs.readdir(modulePath, (err, files) => {
			if (err) {
				reject(`ERR>>> Failed to get a file list from a folder '${modulePath}'`);
			} else {
				resolve(files);
			}
		});
	});
}

function printErrorMessage(errText) {
	console.log(errText);
	rl.close();
}

function initMakeBlock(candidateBlockName) {
	const moduleNames = candidateBlockName.trim().split(/\s+/);

	const makeBlock = moduleName => {
		const modulePath = path.join(BLOCKS_DIR, moduleName);

		return validateBlockName(moduleName)
			.then(() => directoryExist(modulePath, moduleName))
			.then(() => createDir(modulePath))
			.then(() => createDir(modulePath + '/images'))
			.then(() => createFiles(modulePath, moduleName))
			.then(() => getFiles(modulePath))
			.then(files => {
				const line = '-'.repeat(48 + moduleName.length);
				console.log(line);
				console.log(`The module has just been created in 'app/modules/${moduleName}'`);
				console.log(line);

				// Displays a list of files created
				files.forEach(file => console.log(file));

				rl.close();
			});
	};

	if (moduleNames.length === 1) {
		return makeBlock(moduleNames[0]);
	}

	const promises = moduleNames.map(name => makeBlock(name));
	return Promise.all(promises);
}

//
// Start here
//

// Command line arguments
const moduleNameFromCli = process.argv
		.slice(2)
		// join all arguments to one string (to simplify the capture user input errors)
		.join(' ');

// If the user pass the name of the module in the command-line options
// that create a module. Otherwise - activates interactive mode
if (moduleNameFromCli !== '') {
	initMakeBlock(moduleNameFromCli).catch(printErrorMessage);
} else {
	rl.setPrompt('Block(s) name: ');
	rl.prompt();
	rl.on('line', (line) => {
		initMakeBlock(line).catch(printErrorMessage);
	});
}
